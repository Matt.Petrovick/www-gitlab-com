---
layout: markdown_page
title: "Handbook Roadmap"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

This page will be for documenting and sharing plans for the handbook in true GitLab fashion: [iteratively](/handbook/values/#iteration) and [transparently](/handbook/values/#transparency). Like the handbook overall, this roadmap will _always_ be a work in progress—a [living document](https://en.wikipedia.org/wiki/Living_document).

## Ongoing work

Small improvements such as link updates and text tweaks are constantly being made, but small changes can be made as quickly as updating the roadmap, so they don't need to be mentioned here unless they are part of a larger effort.

## Larger efforts

This is a work in progress.

### Refactoring

This section is a stub. It will be expanded soon.