---
layout: markdown_page
title: "Sourcing"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Purpose of Sourcing

The speed with which we can grow our team is dependent on the rate at which we
get qualified applicants. If we have capacity to interview more applicants than
we are receiving organically, we may resort to active sourcing in an attempt to
close the gap.

The purpose of sourcing is to generate a large list of qualified potential
candidates. From this list, Recruiting will solicit interest, and we hope to
increase the number of qualified candidates in our pipeline.

## Sourcing vs. Referrals

Sourcing and [making
referrals](/handbook/hiring/greenhouse/#making-a-referral)
are similar activities, because they both involve recommending people to join
the company. They differ in familiarity and confidence, however:

- If you make a **referral**, you are claiming a personal relationship with the
  candidate that enables you to make a very confident claim about their cultural
  fit with GitLab and their ability to excel at the role in question.
- If you **source** a candidate, you do not have to know them or have any
  confidence in their ability to excel at the role beyond "they seem qualified."

Because of this difference, sourcing is usually not a very detailed activity,
and it is common for a person to source dozens of candidates in a brief session
where they may rarely if ever make a referral. This also means that [referral
bonuses](/handbook/incentives/#referral-bonuses) and
other incentive programs for referrals do not apply to sourced candidates.

## Sourced vs. Prospect

**Sourced** - a candidate who is IQA'd (Intersted, Qualifed, Available).  This candidate has shown interest and is scheduled to speak with someone (Recruiter/Hiring Manager) at GitLab.   
**Prospect** - a candidate that looks good based on the information we have available (LinkedIn, GitHub, Blogs, Conference Talks, etc...).  The candidate has been reached out to but we do not have any response or anything scheudled with them.  These should be marked as prospects when added to Greenhouse.  

## How to Source Candidates

Sourcing candidates doesn’t mean that you should know these people in person.
For anyone that you do not have a personal relationship with, simply add them as
a Prospect to Greenhouse. To do this you should go to Greenhouse, click on Add a 
candidate and then switch to the Prospect tab. This means that these people won’t
be counted as your referrals. Once a candidate expresses their interest in our 
openings, you can convert a Prospect to a Candidate status in Greenhouse.
If you have anyone that you know or are familiar with that you think would be a
good candidate, please make sure to submit them to Greenhouse as a referral (you can
ask a Recruiter for a link).

The most efficient way to source candidates for any position is to search
through a professional network, such as LinkedIn, AngelList, etc. for people who
match the skillset and job history that you are looking for. For some positions,
other networks may prove useful as well - for example, someone sourcing for a
developer role could search GitLab profiles to identify promising candidates.
Professional networks however make it easy to scan a person's employment history
quickly and efficiently and are designed to present their best impression to
potential employers. Because of this, they are a very efficient tool for finding
potential candidates.

When you have identified someone as a good potential candidate, send their
profile along with any requested information to your Sourcing partner in
recruiting so they can reach out to the candidate. You can check the Sourcer alignment
[here](https://about.gitlab.com/handbook/hiring/recruiting-alignment/).
If you want to reach out to a sourced candidate directly you can refer to our 
[reach out templates](https://docs.google.com/presentation/d/1ySqgLoYnFUGtb7hdywav6iSb_NBPRhfIs6WZlGne6Ww/edit?usp=sharing) or
create your own message. Kidnly discuss your communication strategy with your
Sourcing partner to avoid duplication and poor candidate experience.

**Diversity** - In accordance to our [values](https://about.gitlab.com/company/culture/inclusion/#values) of diversity and inclusion, and to build teams as diverse as our users, Recruiting provides a collection of diversity sourcing tools [here](https://docs.google.com/spreadsheets/d/1Hs3UVEpgYOJgvV8Nlyb0Cl5P6_8IlAlxeLQeXz64d8Y/edit#gid=2017610662) to expand our hiring teams’ candidate pipelines. If you have any questions on how to implement these resources into your current sourcing strategy or have suggestions for new tools, please reach out to Recruiting. 

## Upgrading your LinkedIn account

We're happy to tool up our hiring managers and all the team members involved in hiring
with the needed sourcing licenses. 
You can request a *Hiring Manager seat* to be able to collaborate with your Recruiters/Sourcers
on LinkedIn.You can also request the upgrade to a *Recruiter seat* if you're actively involved
in sourcing.
Kindly note that you should ask your Sourcer/Recruiter to upgrade your account.
We won't be able to reimburse any LinkedIn seats purchased at your own expense.
Please make sure to associate your GitLab email with your LinkedIn profile before
requesting a new Hiring Manager or Recruiter seat.

## Example: Sourcing Developers

Here's an example workflow for sourcing developer candidates:

1. In LinkedIn, search for `"ruby on rails" developer`. LinkedIn presents you
   with a list of matching profiles.
1. Based on their current company/job title, open any profiles that look
   interesting in a new tab.
1. Review the candidate's job history and skillset for positive indicators,
   like:
     * Multiple endorsements for Ruby
     * Tenure at established/strong Rails companies
     * Startup/product experience
     * ...or anything else that makes them seem likely to be a fit for our team
1. If they seem like a good fit, pass their information to the Recruiting team
by adding them as a Candidate to Greenhouse. To do so, please login to Greenhouse
and click on *Add a candidate* on the top right. Once a candidate is submitted, 
please ping any Sourcer (all information about our Sourcing Team are linked on the
[team page](https://about.gitlab.com/company/team/)), by @ mentioning them in the
notes section in Greenhouse - this will ensure we reach out to them! 
If you need any help with adding a candidate to Greenhouse - kindly reach out to
any of our Sourcers.

Following this method - and with practice scanning profiles - it's possible to
find a few dozen potential good candidates in 30 minutes or less.

## Source-A-Thons

### Intro

As our company continues to see rapid growth, we need to aim to hire the best
talent out there and we want all GitLab team-member’s to participate with the Recruiting
team in building the greatest GitLab crew!
We ask you all to contribute by digging into LinkedIn or your favorite social
media space and add candidates to the spreadsheet.
By doing this, we are reaching out to people that are very closely aligned with
the team’s needs and finding better-suited candidates, instead of just waiting
for ad-response! (We call that, “the Post and Pray” model).

### Source-a-thon participants:

* Recruiter and/or Recruiting Sourcer
* Hiring manager
* Members of the team we’re sourcing for
* All other team members willing to participate are welcome!

### How to organize:

* Hiring manager requests Recruiter/Recruiting Sourcer to organise a Source-a-thon stating the desired time and participant list
* Recruiter/Recruiting Sourcer schedules a calendar event, adds Zoom link and a sourcing spreadsheet template available [here](https://docs.google.com/spreadsheets/d/1Zy4n5MpuyhkucrF363ZyJq6oNDEYApkAqia-eDMmtFE/edit).
* Recruiter/Recruiting Sourcer should add a source-a-thon spreadsheet to this [folder](https://drive.google.com/drive/folders/164enfXxW_G4Z562ZHT-VNqCTdZUzGDhS?usp=sharing).

### Timing:

* Source-a-thon is limited by 30 minutes
* First 5 minutes is devoted to role description - Hiring manager should share must-haves/nice-to-haves with the team and answer all the questions regarding the role
* All other time is devoted to sourcing and adding profiles into the spreadsheet
* We expect that team members can still add sourced candidates during 24 hours after the source-a-thon
* 1 day after the meeting Recruiting Sourcer will reach out to all the candidates in the spreadsheet
If any sourced candidate is added more than 1 day after the source-a-thon you should ping Recruiting Sourcer in the comments.

### Source-a-thon targets

* Manager can set up a requirement for the minimum number of candidates each participant should add
* Manager can also require his/her team to attend a source-a-thon or source candidates prior to/24 hours after the meeting.

## What We Do With Potential Candidates

When you source a candidate and provide their information to our recruiting team,
you can expect that they will receive an email asking them if they’re interested
in talking about an opportunity to join our team. You won’t be involved or
mentioned in this process aside from having passed their information along.

We do try to personalize these emails as much as feasibly possible so that they
are not impersonal or "spammy," and we rely on the team sourcing to identify
relevant and qualified candidates to ensure these messages are as meaningful as
possible.
