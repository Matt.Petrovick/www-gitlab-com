---
layout: job_family_page
title: "Candidate Experience Specialist"
---

# Candidate Experience Specialist 

The GitLab Candidate Experience Specialists works to create positive experiences for GitLab candidates and hiring teams. In a growing fast-paced environment, the Candidate Experience Specialist is a dynamic team member who helps create an amazing candidate experience, improve our existing hiring practices, and deliver exceptional results to our hiring teams. GitLab strives to be a preferred employer and relies on the Candidate Experience Specialists to act as brand ambassadors by embodying the company values during every interaction with potential team members. The Candidate Experience Specialist is enthusiastic about working with high volume and is dedicated to helping GitLab build a qualified, diverse, and motivated team. The Candidate Experience Specialist reports to the Candidate Experience Manager. 

## Responsibilities

* Collaborate with Recruiters and Recruiting Manager to understand the process for each open vacancy
* Work in Applicant Tracking System (ATS) to help recruiters maintain a positive candidate experience for candidates
* Manage inbound candidate traffic for all roles
* Open and close vacancies through GitLab with the coordination of ATS
* Act as the Recruiting Coordinator for the interview process
* Assist with conducting reference checks for all candidates entering the final round of interview phase
* Build an effective network of internal and external resources to call on as needed
* Ensure candidates receive timely, thoughtful and engaging messaging throughout the hiring process
* Assist Recruiting team with putting together GitLab hiring packages and corresponding with hiring managers and CEO for proper approvals
* Create and stage offer letter and employment contracts for recruiting team
* Assist with Onboarding processes, including ordering equipment and supplies for team members
* Promote our values, culture and remote only passion

## Requirements

* Desire to learn about Talent Acquisition and Human Resources from the ground level up
* Demonstrated ability to work in a team environment and work with C-Suite level candidates Focus on delivering an excellent candidate experience
* Ambitious, efficient and stable under tight deadlines and competing priorities
* Proficient in Google Suite
* Ability to build relationships with managers and colleagues across multiple disciplines and timezones
* Outstanding written and verbal communication skills across all levels
* Willingness to learn and use software tools including Git and GitLab
* Organized, efficient, and proactive with a keen sense of urgency
* Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks).
* Excellent communication and interpersonal skills
* Prior experience using an candidate tracking system, Greenhouse experience is a plus
* Ability to recognize and appropriately handle highly sensitive and confidential information
* Experience working remotely is a plus
* You share our [values](/handbook/values), and work in accordance with those values.

# Candidate Experience Manager 

This role reports to the Senior Director of Recruiting. 

## Responsibilities

* Lead a collaborative remote based candidate experience team that can scale to the dynamic demands of a rapidly growing world-wide technology company
* Provide coaching to improve performance of team members and drive accountability
* Serve as the primary coordinator(~50% of capacity) for senior and executive level roles, agency relationships, and Board of Directors Nomination and Government Committee
* Remove operational roadblocks by troubleshooting and enabling scalable solutions in collaboration with stakeholders
* Identify  process improvement opportunities and implement changes in a fast-paced environment, consistently documenting in the handbook for institutional knowledge
* Onboard, mentor, and grow the careers of all team members
* Leverage  data from our applicant tracking system and scheduling tool to help drive decisions
* Build trusted partnerships with recruiting leaders, People Operations team, Legal, IT, and Department Leaders  to collaborate and drive alignment 
* Leverage our candidate facing coordination team to market our employer brand to showcase that GitLab is a world-class place to work and ensuring that everyone on the team is a champion of our brand
* Help define consistent data-driven hiring metrics and deliver on the goals
* Partner closely with People Operations team to ensure a white glove handoff

## Requirements

* Exceptional cross-functional communication and organization skills, and demonstrated experience in time-management and ability to influence
* A creative mindset to get things done effectively and efficiently
* Familiarity with an ATS 
* BS/BA degree
* Leadership or performance management experience
* Ability and passion to lead, mentor, and support the candidate experience team
* Strong time management, communication and organizational skills
* A team player with excellent client management skills
