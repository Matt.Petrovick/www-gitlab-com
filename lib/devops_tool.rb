require 'yaml'

module Gitlab
  module Homepage
    class DevopsTool
      attr_reader :key

      def initialize(key, data)
        @key = key
        @data = data
      end

      def gitlab?
        @key[0..6] == 'gitlab_'
      end

      # rubocop:disable Style/MethodMissingSuper
      # rubocop:disable Style/MissingRespondToMissing

      ##
      # Middeman Data File objects compatibility
      #
      def method_missing(name, *args, &block)
        @data[name.to_s]
      end

      # rubocop:enable Style/MethodMissingSuper
      # rubocop:enable Style/MissingRespondToMissing

      def self.all!
        @features ||= YAML.load_file('data/features.yml')
        @features['devops_tools'].map do |key, data|
          new(key, data)
        end
      end

      def self.for_stage(stage)
        all!.dup.keep_if do |devops_tool|
          !devops_tool.gitlab? && devops_tool.category && !(devops_tool.category & stage.categories.map(&:key)).empty?
        end
      end
    end
  end
end
